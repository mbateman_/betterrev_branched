package org.adoptopenjdk.betterrev.controllers;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Bean;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.adoptopenjdk.betterrev.models.Contribution;
import org.adoptopenjdk.betterrev.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
@Path("contributions")
public class ContributionsController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ContributionsController.class); 

    // TODO Figure out what this is
    @Context
    private ResourceContext resourceContext;

    // TOOD We will remove this BS map
    Map<Long, Contribution> mockContributions;

    @PostConstruct
    // TODO - Remove this later once we have the real data from the data store
    public void init() {
        this.mockContributions = new ConcurrentHashMap<>();
        User user = new User("karianna", "Martijn Verburg", User.OcaStatus.SIGNED);
        Contribution contribution = new Contribution("jdk9", "1", "Test Contribution", "New Contribution", user, LocalDateTime.now(), LocalDateTime.now(), "jdk9");
        this.mockContributions.putIfAbsent(1L, contribution);
   }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Contribution> allBeans() {
        return mockContributions.values();
    }

    @Path("{contributionId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Contribution getContributionById(@PathParam("contributionId") Long contributionId) {
        // TODO get a real contribution
        LOGGER.info("get call arrived");
        return mockContributions.get(contributionId);
    }

    
}
