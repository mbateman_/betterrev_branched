package org.adoptopenjdk.betterrev.update.ocachecker;

import static org.adoptopenjdk.betterrev.models.ContributionEventType.OCA_SIGNED;

import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.ContributionEventType;
import org.adoptopenjdk.betterrev.models.User;
import org.adoptopenjdk.betterrev.models.User.OcaStatus;
import org.adoptopenjdk.betterrev.update.BetterrevActor;

public class OCASignedChecker extends BetterrevActor {
    
    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof ContributionEvent)) {
            return;
        }
        ContributionEvent request = (ContributionEvent) message;
        if (request.contributionEventType != ContributionEventType.CONTRIBUTION_GENERATED) {
            return;
        }

        checkOcaStatus(request);
    }

    private void checkOcaStatus(ContributionEvent request) {
        boolean ocaSign = CheckOcaStatus.check(request);
        if (ocaSign) {
            publishNotificationEvent(request);
        } else {
            alertOcaSignMissing(request);
        }
    }

    private void alertOcaSignMissing(ContributionEvent request) {
        // TODO #65 Send an email (waiting implementation of issue
        // #65)
    }

    private void publishNotificationEvent(ContributionEvent request) {
        User user = request.contribution.requester;
        user.ocaStatus = OcaStatus.SIGNED;
        // TODO Save the user
        // user.save();
        ContributionEvent ocaNotifiedEvent = new ContributionEvent(OCA_SIGNED,
                request.contribution);
        eventStream().publish(ocaNotifiedEvent);
    }

}
