/**
 * A service factory which creates an object with methods for fetching 
 * contributions from the REST service.
 */
betterrevApp.factory('contributionsService', ['$resource', 'environment', function ($resource, environment) {
        return {
            getAll: function() {
                return $resource(environment.baseURL + 'betterrev/contributions').query();
            }//,
            /*get: function(contributionId) {
                // return $resource(environment.baseURL + 'betterrev/contributions/' + contributionId).query();
                
                
                return $resource(environment.baseURL + 'betterrev/contributions/' + contributionId, 
                     // {contributionId: '@id'}, 
                     {'query':  {method:'GET', isArray:false}});
            }*/
        };
    }]);

