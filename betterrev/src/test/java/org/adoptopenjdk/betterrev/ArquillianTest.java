package org.adoptopenjdk.betterrev;

import org.adoptopenjdk.betterrev.models.Tag;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.archive.importer.MavenImporter;
import org.jboss.shrinkwrap.resolver.api.maven.archive.importer.PomEquippedMavenImporter;
import org.jboss.shrinkwrap.resolver.api.maven.strategy.MavenResolutionStrategy;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Basic Tag persistence tests.
 * TODO: Tidy tests so they have one assert each
 * 
 * TODO could insert data from "initial-data.yml"
 */
@RunWith(Arquillian.class)
public abstract class ArquillianTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArquillianTest.class);

    // Arquillian Magic to deploy this to a minimal WAR in a container
    @Deployment
    public static Archive<?> createDeployment() throws Exception {

        /*
        WebArchive war2
        = ShrinkWrap.create(MavenImporter.class)
        .loadPomFromFile("pom.xml")
        .importBuildOutput()
        .as(WebArchive.class);
        */
        
        WebArchive war = 
                ShrinkWrap.create(WebArchive.class, "test.war")
                .addClass(ArquillianTest.class)
                .addClass(ArquillianTransactionalTest.class)
                .addClass(Tag.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("test-logback.xml", "logback.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        
        LOGGER.debug(war.toString(true));
        
        return war;
    }

}
